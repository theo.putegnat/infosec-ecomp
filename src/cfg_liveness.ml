open Batteries
open Cfg

(* Analyse de vivacité *)

(* [vars_in_expr e] renvoie l'ensemble des variables qui apparaissent dans [e]. *)
let rec vars_in_expr (e: expr) : string Set.t = (*Type de sortie rajouté par moi*)
   (* TODO *)
   match e with 
   | Ebinop (b,e1,e2) -> Set.union (vars_in_expr e1)  (vars_in_expr e2)
   | Eunop (u,e) -> vars_in_expr e
   | Eint i -> Set.empty
   | Evar s -> Set.singleton s
   | Ecall (fname,args) -> List.fold (fun acc arg -> Set.union acc (vars_in_expr arg)) Set.empty args

(* [live_after_node cfg n] renvoie l'ensemble des variables vivantes après le
   nœud [n] dans un CFG [cfg]. [lives] est l'état courant de l'analyse,
   c'est-à-dire une table dont les clés sont des identifiants de nœuds du CFG et
   les valeurs sont les ensembles de variables vivantes avant chaque nœud. *)
   let live_after_node cfg n (lives: (int, string Set.t) Hashtbl.t) : string Set.t =
      (* TODO *)
      Set.fold ((fun x acc-> 
         match Hashtbl.find_option lives x with 
         | Some set -> Set.union acc set
         | None -> acc
         )  )  (succs cfg n ) Set.empty


(* [live_cfg_node node live_after] renvoie l'ensemble des variables vivantes
   avant un nœud [node], étant donné l'ensemble [live_after] des variables
   vivantes après ce nœud. *)
let live_cfg_node (node: cfg_node) (live_after: string Set.t) =
   (* TODO *)
   match node with 
   | Cassign (s, e, i) -> Set.union (vars_in_expr e) (Set.diff live_after (Set.singleton s))
   | Creturn e -> vars_in_expr e
   | Ccmp (e,i1,i2) -> Set.union (vars_in_expr e) live_after
   | Cnop i -> live_after
   | Ccall (fname,args,s) -> 
      (List.fold (fun acc arg -> Set.union acc (vars_in_expr arg)) live_after args) 

(* [live_cfg_nodes cfg lives] effectue une itération du calcul de point fixe.

   Cette fonction met à jour l'état de l'analyse [lives] et renvoie un booléen
   qui indique si le calcul a progressé durant cette itération (i.e. s'il existe
   au moins un nœud n pour lequel l'ensemble des variables vivantes avant ce
   nœud a changé). *)
let live_cfg_nodes cfg (lives : (int, string Set.t) Hashtbl.t) =
   (* TODO *)
   List.fold (fun acc (node_int, node) -> 
      let list_out = live_after_node cfg node_int lives in
      let list_in = live_cfg_node node list_out in
      match (Hashtbl.find_option lives node_int) with
      | Some list_of_nodes -> 
         if Set.equal list_in list_of_nodes then acc
         else 
            (Hashtbl.replace lives node_int list_in ; true)
      | None -> Hashtbl.replace lives node_int list_in ; true)
       false (List.sort compare @@ Hashtbl.to_list cfg) 

(* [live_cfg_fun f] calcule l'ensemble des variables vivantes avant chaque nœud
   du CFG en itérant [live_cfg_nodes] jusqu'à ce qu'un point fixe soit atteint.
   *)
let live_cfg_fun (f: cfg_fun) : (int, string Set.t) Hashtbl.t =
  let lives = Hashtbl.create 17 in
     (* TODO *)
     let rec boucle_while_cachee cfg = if live_cfg_nodes cfg lives 
      then boucle_while_cachee cfg
   else lives in
   boucle_while_cachee f.cfgfunbody