open Elang
open Batteries
open Prog
open Utils
open Builtins

let binop_bool_to_int f x y = if f x y then 1 else 0

(* [eval_binop b x y] évalue l'opération binaire [b] sur les arguments [x]
   et [y]. *)
let eval_binop (b: binop) : int -> int -> int =
  match b with
   | Eadd -> fun x y -> x + y
   | Emul -> fun x y -> x * y
   | Emod -> fun x y -> x mod y
   | Exor -> fun x y -> x lxor y
   | Ediv -> fun x y -> x / y
   | Esub -> fun x y -> x - y
   | Eclt -> fun x y -> binop_bool_to_int (fun x y -> x < y) x y
   | Ecle -> fun x y -> binop_bool_to_int (fun x y -> x <= y) x y 
   | Ecgt -> fun x y -> binop_bool_to_int (fun x y -> x > y) x y
   | Ecge -> fun x y -> binop_bool_to_int (fun x y -> x >= y) x y
   | Eceq -> fun x y -> binop_bool_to_int (fun x y -> x = y) x y
   | Ecne -> fun x y -> binop_bool_to_int  (fun x y -> not(x = y)) x y 

(* [eval_unop u x] évalue l'opération unaire [u] sur l'argument [x]. *)
let eval_unop (u: unop) : int -> int =
  match u with
   | Eneg -> fun x -> -x

(* [eval_eexpr st e] évalue l'expression [e] dans l'état [st]. Renvoie une
   erreur si besoin. *)
let rec eval_eexpr ep oc st (e : expr) : int res =
   begin
   match e with 
   | Eint x-> OK (x)
   | Evar x -> 
      begin 
      match Hashtbl.find_option st.env x with
         | Some a -> OK (a)
         | None -> Error "Une erreur dans eval_eexpr, on attend une variable"
      end
   | Ecall (fname,args) -> 
      find_function ep fname >>! (fun f ->
      eval_efun ep oc st f fname (List.map (fun arg -> eval_eexpr ep oc st arg >>! fun valeur -> valeur) args)
       >>! (fun (fun_result, new_state) ->  
      (match fun_result with 
      | Some result -> OK (result)
      | None -> Error ("On attend une valeur de retour pour cet appel de fonction"))))
         
   | Ebinop (b, e1, e2) -> 
      eval_eexpr ep oc st e1 >>! (fun expr1 -> 
      eval_eexpr ep oc st e2 >>! (fun expr2 -> 
      OK (eval_binop b expr1 expr2)))
   | Eunop (b, e) -> 
      eval_eexpr ep oc st e >>! (fun expr ->
         OK (eval_unop b expr))  
      end     

(* [eval_einstr oc st ins] évalue l'instrution [ins] en partant de l'état [st].

   Le paramètre [oc] est un "output channel", dans lequel la fonction "print"
   écrit sa sortie, au moyen de l'instruction [Format.fprintf].

   Cette fonction renvoie [(ret, st')] :

   - [ret] est de type [int option]. [Some v] doit être renvoyé lorsqu'une
   instruction [return] est évaluée. [None] signifie qu'aucun [return] n'a eu
   lieu et que l'exécution doit continuer.

   - [st'] est l'état mis à jour. *)
and eval_einstr ep oc (st: int state) (ins: instr) :
  (int option * int state) res =
  begin
  match ins with 
  | Iassign (key, expr) -> 
   begin
      match (Hashtbl.find_option st.env key) with 
      | Some v -> eval_eexpr ep oc st expr >>! fun expr_evaluated -> 
         let _ = Hashtbl.replace st.env key (expr_evaluated) in OK (None,st)
      | None -> eval_eexpr ep oc st expr >>! fun expr_evaluated -> 
         let _ = Hashtbl.add st.env key expr_evaluated in OK (None, st)
   end

  | Iif (expr,i1,i2) -> 
      if eval_eexpr ep oc st expr = OK (1) 
         then eval_einstr ep oc st i1 
      else eval_einstr ep oc st i2

  | Iwhile (expr,instr) -> 
      if eval_eexpr ep oc st expr = OK(1) 
         then 
      eval_einstr ep oc st instr >>= fun (ret,nstate) -> 
      if ret = None then eval_einstr ep oc nstate ins else OK (ret, nstate)
   else OK (None, st)
      
  | Iblock (instr_list) -> 
   begin
      match instr_list with
      | [] -> OK (None, st)
      | i1::r -> eval_einstr ep oc st i1 >>= (fun (ret,new_state) ->
            if ret = None then 
               eval_einstr ep oc new_state (Iblock r) 
         else 
               OK (ret, new_state))
   end
  | Ireturn e -> 
      eval_eexpr ep oc st (e) >>= fun expr_evaluated -> OK (Some (expr_evaluated), st)
  | Icall (funame,args) -> 
   match do_builtin oc st.mem funame (List.map (fun arg -> eval_eexpr ep oc st arg >>! fun valeur -> valeur) args) with
   | OK Some i -> Error "Pas de retour ici"
   | OK None -> OK (None, st)
   | Error _ -> 
      find_function ep funame >>! (fun f ->
         eval_efun ep oc st f funame (List.map (fun arg -> eval_eexpr ep oc st arg >>! fun valeur -> valeur) args) >>! 
         (fun (fun_result, new_state) -> 
            OK (None,new_state)
               ))
   end
(* [eval_efun oc st f fname vargs] évalue la fonction [f] (dont le nom est
   [fname]) en partant de l'état [st], avec les arguments [vargs].

   Cette fonction renvoie un couple (ret, st') avec la même signification que
   pour [eval_einstr]. *)
and eval_efun ep oc (st: int state) ({ funargs; funbody}: efun) (fname: string) (vargs: int list)
  : (int option * int state) res =
  (* L'environnement d'une fonction (mapping des variables locales vers leurs
     valeurs) est local et un appel de fonction ne devrait pas modifier les
     variables de l'appelant. Donc, on sauvegarde l'environnement de l'appelant
     dans [env_save], on appelle la fonction dans un environnement propre (Avec
     seulement ses arguments), puis on restore l'environnement de l'appelant. *)
  let env_save = Hashtbl.copy st.env in
  let env = Hashtbl.create 17 in
  match List.iter2 (fun a v -> Hashtbl.replace env a v) funargs vargs with
  | () ->
    eval_einstr ep oc { st with env } funbody >>= fun (v, st') ->
    OK (v, { st' with env = env_save })
  | exception Invalid_argument _ ->
    Error (Format.sprintf
             "E: Called function %s with %d arguments, expected %d.\n"
             fname (List.length vargs) (List.length funargs)
          )

(* [eval_eprog oc ep memsize params] évalue un programme complet [ep], avec les
   arguments [params].

   Le paramètre [memsize] donne la taille de la mémoire dont ce programme va
   disposer. Ce n'est pas utile tout de suite (nos programmes n'utilisent pas de
   mémoire), mais ça le sera lorsqu'on ajoutera de l'allocation dynamique dans
   nos programmes.

   Renvoie:

   - [OK (Some v)] lorsque l'évaluation de la fonction a lieu sans problèmes et renvoie une valeur [v].

   - [OK None] lorsque l'évaluation de la fonction termine sans renvoyer de valeur.

   - [Error msg] lorsqu'une erreur survient.
   *)
let eval_eprog oc (ep: eprog) (memsize: int) (params: int list)
  : int option res =
  let st = init_state memsize in
  find_function ep "main" >>= fun f ->
  (* ne garde que le nombre nécessaire de paramètres pour la fonction "main". *)
  let n = List.length f.funargs in
  let params = take n params in
  eval_efun ep oc st f "main" params >>= fun (v, _) ->
  OK v
