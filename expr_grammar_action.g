tokens SYM_EOF SYM_IDENTIFIER<string> SYM_INTEGER<int> SYM_PLUS SYM_MINUS SYM_ASTERISK SYM_DIV SYM_MOD
tokens SYM_LPARENTHESIS SYM_RPARENTHESIS SYM_LBRACE SYM_RBRACE
tokens SYM_ASSIGN SYM_SEMICOLON SYM_RETURN SYM_IF SYM_WHILE SYM_ELSE SYM_COMMA
tokens SYM_EQUALITY SYM_NOTEQ SYM_LT SYM_LEQ SYM_GT SYM_GEQ
non-terminals S INSTR INSTRS LINSTRS ELSE EXPR FACTOR
non-terminals LPARAMS REST_PARAMS
non-terminals IDENTIFIER INTEGER
non-terminals FUNDEF FUNDEFS
non-terminals ADD_EXPRS ADD_EXPR
non-terminals MUL_EXPRS MUL_EXPR
non-terminals CMP_EXPRS CMP_EXPR
non-terminals EQ_EXPRS EQ_EXPR
non-terminals FUNCALL LARGS REST_ARGS DEFVAR_OR_FUNCALL VAR_OR_FUNCALL

axiom S
{

  open Symbols
  open Ast
  open BatPrintf
  open BatBuffer
  open Batteries
  open Utils

  (* TODO *)
  let rec resolve_associativity term other =
       (* TODO *)
       match other with 
       | [] -> term
       | (op,arg)::r -> resolve_associativity (Node (op,[term]@[arg])) r;

type funcallorAssignvar =
  | Funcall of tree list
  | AssignVar of tree

}

rules
S -> FUNDEFS SYM_EOF {  Node (Tlistglobdef, $1) }

FUNDEFS -> FUNDEF FUNDEFS { $1 :: $2}
FUNDEFS -> {[]}

FUNDEF -> IDENTIFIER SYM_LPARENTHESIS LPARAMS SYM_RPARENTHESIS INSTR {Node (Tfundef, [$1;Node(Tfunargs,$3);$5])}

LPARAMS -> IDENTIFIER REST_PARAMS {Node(Targ,[$1]) :: $2}
LPARAMS -> {[]}

REST_PARAMS -> SYM_COMMA IDENTIFIER REST_PARAMS {Node (Targ,[$2]) :: $3}
REST_PARAMS -> {[]}
 
INSTR -> SYM_IF SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS LINSTRS ELSE { Node (Tif, [$3;$5]@$6)} 
INSTR-> SYM_WHILE SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS INSTR {Node (Twhile, [$3;$5])}
INSTR -> SYM_RETURN EXPR SYM_SEMICOLON {Node (Treturn, [$2])}
INSTR -> IDENTIFIER DEFVAR_OR_FUNCALL {
  match $2 with 
  | AssignVar tree -> Node (Tassign, [Node (Tassignvar,[$1 ; tree])])
  | Funcall treelist-> Node (Tcall, $1 :: [Node (Targs,treelist)])
}
INSTR -> LINSTRS {$1}

DEFVAR_OR_FUNCALL -> FUNCALL SYM_SEMICOLON {Funcall $1}
DEFVAR_OR_FUNCALL -> SYM_ASSIGN EXPR SYM_SEMICOLON {AssignVar $2}

FUNCALL -> SYM_LPARENTHESIS LARGS SYM_RPARENTHESIS {$2}

LARGS -> EXPR REST_ARGS {$1 :: $2}
LARGS -> {[]}

REST_ARGS -> SYM_COMMA EXPR REST_ARGS {$2 :: $3}
REST_ARGS -> {[]}

EXPR -> EQ_EXPR EQ_EXPRS { resolve_associativity $1 $2}

EQ_EXPR -> CMP_EXPR CMP_EXPRS { resolve_associativity $1 $2}

CMP_EXPR -> ADD_EXPR ADD_EXPRS { resolve_associativity $1 $2}

ADD_EXPR -> MUL_EXPR MUL_EXPRS { resolve_associativity $1 $2}

MUL_EXPR -> FACTOR {$1}
MUL_EXPR -> SYM_MINUS FACTOR {Node (Tneg,[$2])}

FACTOR -> INTEGER {$1}
FACTOR -> IDENTIFIER VAR_OR_FUNCALL {
  match $2 with 
  | AssignVar _ -> $1
  | Funcall treelist-> Node (Tcall, $1 :: [Node (Targs,treelist)])}

FACTOR -> SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS {$2}

VAR_OR_FUNCALL -> FUNCALL {Funcall $1}
VAR_OR_FUNCALL -> {AssignVar NullLeaf}


MUL_EXPRS -> SYM_ASTERISK MUL_EXPR MUL_EXPRS { (Tmul,$2) :: $3 }
MUL_EXPRS -> SYM_DIV MUL_EXPR MUL_EXPRS { (Tdiv,$2) :: $3 }
MUL_EXPRS -> SYM_MOD MUL_EXPR MUL_EXPRS { (Tmod,$2) :: $3 }
MUL_EXPRS -> {[]}

ADD_EXPRS -> SYM_PLUS ADD_EXPR ADD_EXPRS { (Tadd,$2) :: $3 }
ADD_EXPRS -> SYM_MINUS ADD_EXPR ADD_EXPRS { (Tsub,$2) :: $3 }
ADD_EXPRS-> {[]}

EQ_EXPRS -> SYM_EQUALITY MUL_EXPR MUL_EXPRS { (Tceq,$2) :: $3 }
EQ_EXPRS -> SYM_NOTEQ MUL_EXPR MUL_EXPRS { (Tne,$2) :: $3 }
EQ_EXPRS -> {[]}

CMP_EXPRS -> SYM_GEQ MUL_EXPR MUL_EXPRS { (Tcge,$2) :: $3 }
CMP_EXPRS -> SYM_GT MUL_EXPR MUL_EXPRS { (Tcgt,$2) :: $3 }
CMP_EXPRS -> SYM_LT MUL_EXPR MUL_EXPRS { (Tclt,$2) :: $3 }
CMP_EXPRS -> SYM_LEQ MUL_EXPR MUL_EXPRS { (Tcle,$2) :: $3 }
CMP_EXPRS -> {[]}


INTEGER -> SYM_INTEGER {Node (Tint, [IntLeaf($1)])}

IDENTIFIER -> SYM_IDENTIFIER {StringLeaf ($1)}

ELSE -> SYM_ELSE LINSTRS {[$2]}
ELSE -> {[]}

LINSTRS -> SYM_LBRACE INSTRS SYM_RBRACE {Node (Tblock,$2)}

INSTRS -> INSTR INSTRS { $1 :: $2}
INSTRS -> {[]}

